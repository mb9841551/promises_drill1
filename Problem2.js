const promise1 = new Promise((resolve) =>
  setTimeout(() => resolve("Hello! I am promise 1"), 1000)
); //creation of promises
const promise2 = Promise.reject(new Error("Error! I am promise 2"));
const promise3 = "Already resolved! I am promise 3";

let arrOfPromises = [promise1, promise2, promise3];

function composePromises(promises) {
  //creation of promise
  return new Promise((resolve) => {
    if (Array.isArray(promises) === false) {
      //to check if array is empty or not
      resolve([]);
      return;
    }
    Promise.allSettled(promises) //using allSettled property to get status of all promises
      .then((results) => {
        resolve(results);
      });
  });
}
composePromises(arrOfPromises) //consumption of promises
  .then((results) => console.log(results))
  .catch((err) => console.error(err));
