function promise1() {
  //Created three promises in three function
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("I am promise 1");
    }, 1000);
  });
}

function promise2() {
  return Promise.resolve("Coming from promise 1 and I am promise 2");
}

function promise3() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("Coming from promise 1 and 2 , I am promise 3");
    }, 2000);
  });
}

let functionsArray = [promise1, promise2, promise3]; //array of promises

function dynamicChain(functionsArray) {
  return new Promise((resolve, reject) => {
    if (!Array.isArray(functionsArray) || functionsArray.length === 0) {
      //to check valid array
      reject(new Error("Invalid array of functions"));
      return;
    }

    let resultPromise = Promise.resolve(); //a promise having no value

    for (let index = 0; index < functionsArray.length; index++) {
      resultPromise = resultPromise.then(functionsArray[index]); //to chain promises that take values from previous promises
    }

    resultPromise
      .then((result) => resolve(result)) // Resolve with the last result directly
      .catch((err) => reject(err));
  });
}

dynamicChain(functionsArray) //consumption of promises
  .then((result) => console.log(result))
  .catch((err) => console.error(err));
