function racePromise1() {
  //Created a promise inside function
  const promise = new Promise((resolve) => {
    let win = "Promise 1 won the race";
    let delay = Math.floor(Math.random() * (3000 - 1000) + 1000); //a random number between 1000 and 3000
    setTimeout(() => {
      resolve(win + " by getting here at " + delay + " millisecond"); //called the resolve function after certain delay
    }, delay);
  });
  return promise;
}

function racePromise2() {
  //Created a promise inside function
  const promise = new Promise((resolve) => {
    let win = "Promise 2 won the race";
    let delay = Math.floor(Math.random() * (3000 - 1000) + 1000); //a random number between 1000 and 3000
    setTimeout(() => {
      resolve(win + " by getting here at " + delay + " millisecond"); //called the resolve function after certain delay
    }, delay);
  });
  return promise;
}

function racePromises() {
  Promise.race([racePromise1(), racePromise2()]) //used race method of Promise to find which promise returns faster
    .then((value) => {
      console.log(value);
    })
    .catch((err) => {
      console.log(err);
    });
}
racePromises();
