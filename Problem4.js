function promise1() {
  //Created 6 promises with setTimeOut in it
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("I am promise 1");
    }, 1000);
  });
}
function promise2() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("I am promise 2");
    }, 1000);
  });
}
function promise3() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("I am promise 3");
    }, 1000);
  });
}
function promise1() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("I am promise 1");
    }, 1000);
  });
}
function promise4() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("I am promise 4");
    }, 1000);
  });
}
function promise5() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("I am promise 5");
    }, 1000);
  });
}
function promise6() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("I am promise 6");
    }, 1000);
  });
}

function paralellLimit(promises, limit) {
  //
  return new Promise((resolve, reject) => {
    if (!Array.isArray(promises)) {
      //to check if promises is valid array or not
      reject(new Error("Not a valid array of promises"));
    }
    const results = [];
    let startIndex = 0;
    function runInLimit() {
      //function to run in specific limit
      let batch = promises.slice(startIndex, startIndex + limit); //used slice to cutoff the aaray in limit
      const batchPromise = Promise.allSettled(batch) //used allSttled to get final state of specified limit of promises
        .then((batchResults) => {
          results.push(...batchResults); //push the promises , used spread to spread into individual elements

          if (startIndex < promises.length) {
            runInLimit(); //run if index is not greater than length of array
          } else {
            resolve(results); //call the resolve function
          }
        })
        .catch((err) => reject(err));

      startIndex += limit; //update the startIndex value after each iteration
      return batchPromise;
    }
    runInLimit(); //call the function
  });
}

let arrOfPromises = [
  promise1(),
  promise2(),
  promise3(),
  promise4(),
  promise5(),
  promise6(),
]; //array of promises

paralellLimit(arrOfPromises, 2) //consumption of promises
  .then((results) => console.log(results))
  .catch((err) => console.error(err));
